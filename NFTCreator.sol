// SPDX-License-Identifier: MIT OR Apache-2.0
pragma solidity 0.8.3;

import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/proxy/Clones.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "./FullTrack.sol";

contract NFTCreator is
    Initializable,
    ReentrancyGuardUpgradeable,
    PausableUpgradeable,
    AccessControlUpgradeable
{
    using Counters for Counters.Counter;
    Counters.Counter private _itemIds;
    Counters.Counter private _itemsSold;
    Counters.Counter private _nftsIds;
    Counters.Counter private _nftsSold;

    address public fullTrackImplementation;

    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");
    bytes32 public constant CREATOR_ROLE = keccak256("CREATOR_ROLE");

    mapping(uint256 => Item) private idToItem;
    mapping(uint256 => mapping(address => bool)) private fanHasNft;
    mapping(address => uint256) private availableTokens;
    mapping(address => mapping(address => uint256)) private royalties;
    mapping(address => address[]) private royaltiesReceivers;

    function initialize(address kloovAdminAddress, address kloovPauserAddress)
        public
        initializer
    {
        require(
            kloovAdminAddress != address(0) && kloovPauserAddress != address(0),
            "NFTCreator: Roles addresses can not be address zero"
        );
        __NFTCreator_init(kloovAdminAddress, kloovPauserAddress);
    }

    function __NFTCreator_init(
        address kloovAdminAddress,
        address kloovPauserAddress
    ) internal initializer {
        __NFTCreator_init_unchained(kloovAdminAddress, kloovPauserAddress);
        __Pausable_init_unchained();
        __AccessControl_init_unchained();
    }

    function __NFTCreator_init_unchained(
        address kloovAdminAddress,
        address kloovPauserAddress
    ) internal initializer {
        _grantRole(DEFAULT_ADMIN_ROLE, kloovAdminAddress);
        _grantRole(PAUSER_ROLE, kloovPauserAddress);
        _grantRole(CREATOR_ROLE, msg.sender);
        FullTrack fullTrack = new FullTrack();
        fullTrackImplementation = address(fullTrack);
    }

    struct Item {
        uint256 itemId;
        address nftContract;
        uint256 supply;
        address payable artist;
        address[] owners;
        uint256 price;
    }

    event ItemsCreated(
        uint256 indexed itemId,
        address indexed nftContract,
        uint256 indexed supply,
        address artist,
        uint256 price
    );

    function getAvailableTokens(address nftContract)
        public
        view
        returns (uint256)
    {
        return availableTokens[nftContract];
    }

    function getNumberOfNFTs() public view returns (uint256) {
        uint256 nftsCount = _nftsIds.current();
        return nftsCount;
    }

    /* Places a group of items for sale on Kloov*/
    function createItem(
        address artist,
        uint256 supply,
        uint256 price,
        address[] memory royaltyAddresses,
        uint256[] memory royaltyAmounts,
        FullTrack.InitializationData memory initializationData
    ) public payable nonReentrant whenNotPaused onlyRole(CREATOR_ROLE) {
        require(price > 0, "NFTCreator: Price must be at least 1 wei");
        require(
            artist != address(0),
            "NFTCreator: Artist address must not be zero address"
        );
        require(
            supply > 0,
            "NFTCreator: Amount of tokens must be greater than zero"
        );

        _itemIds.increment();
        address[] memory owners = new address[](supply);

        address nftContract = Clones.clone(fullTrackImplementation);
        bool success = FullTrack(nftContract).initialize(initializationData);
        require(success, "NFTCreator: Clone intialization failed");

        idToItem[_itemIds.current()] = Item(
            _itemIds.current(),
            nftContract,
            supply,
            payable(artist),
            owners,
            price
        );

        uint256 total;
        for (uint256 i = 0; i < royaltyAddresses.length; i++) {
            require(
                royaltyAddresses[i] != address(0) && royaltyAmounts[i] > 0,
                "NFTCreator: Invalid royalties data"
            );
            royalties[nftContract][royaltyAddresses[i]] = royaltyAmounts[i];
            royaltiesReceivers[nftContract] = royaltyAddresses;
            total += royaltyAmounts[i];
        }
        require(total == 100, "NFTCreator: Royalties must add up to 100%");

        availableTokens[nftContract] = supply;
        _nftsIds.increment();

        emit ItemsCreated(
            _itemIds.current(),
            nftContract,
            supply,
            artist,
            price
        );
    }

    /* Creates the sale of an item */
    /* Transfers ownership of the item, as well as funds between parties */
    function createSale(
        address nftContract,
        uint256 itemId,
        FullTrack.NFTData memory tokenData
    ) public payable nonReentrant whenNotPaused {
        require(
            itemId <= _itemIds.current(),
            "NFTCreator: Create sale for non existend itemId"
        );
        uint256 price = idToItem[itemId].price;
        require(
            msg.value == price,
            "NFTCreator: Please submit the asking price in order to complete the purchase"
        );
        require(
            nftContract != address(0),
            "NFTCreator: NFT address must not be zero address"
        );
        require(
            availableTokens[nftContract] > 0,
            "NFTCreator: Item has already been sold"
        );
        uint256 nextTokenId = idToItem[itemId].supply -
            availableTokens[nftContract] +
            1;
        FullTrack fullTrack = FullTrack(nftContract);
        require(
            fullTrack.redeem(msg.sender, nextTokenId, tokenData),
            "NFTCreator: NFT lazy mint failed"
        );

        uint256 total;
        for (uint256 i = 0; i < royaltiesReceivers[nftContract].length; i++) {
            uint256 royalty = royalties[nftContract][
                royaltiesReceivers[nftContract][i]
            ];
            total += royalty;
            (bool success, ) = payable(royaltiesReceivers[nftContract][i]).call{
                value: (msg.value * royalty) / 100
            }("");
            require(success, "NFTCreator: Transfer failed");
        }
        require(total == 100, "NFTCreator: Royalties must add up to 100%");

        idToItem[itemId].owners[nextTokenId - 1] = payable(msg.sender);
        availableTokens[nftContract] -= 1;
        fanHasNft[itemId][msg.sender] = true;
        if (availableTokens[nftContract] == 0) {
            _itemsSold.increment();
        }
    }

    /* Returns all available NFTs*/
    function fetchUnsoldNFTs() public view returns (Item[] memory) {
        uint256 itemsCount = _itemIds.current();
        uint256 unsoldItemsCount = itemsCount - _itemsSold.current();
        uint256 currentIndex = 0;

        Item[] memory items = new Item[](unsoldItemsCount);
        for (uint256 i = 0; i < itemsCount; i++) {
            if (availableTokens[idToItem[i + 1].nftContract] > 0) {
                uint256 currentId = i + 1;
                Item storage currentItem = idToItem[currentId];
                items[currentIndex] = currentItem;
                currentIndex += 1;
            }
        }
        return items;
    }

    /* Returns only NFTs that a user has purchased */
    function fetchMyNFTs() public view returns (Item[] memory) {
        uint256 totalItemCount = _itemIds.current();
        uint256 itemCount = 0;
        uint256 currentIndex = 0;

        for (uint256 i = 0; i < totalItemCount; i++) {
            if (fanHasNft[i + 1][msg.sender]) {
                itemCount += 1;
            }
        }

        Item[] memory items = new Item[](itemCount);

        for (uint256 i = 0; i < totalItemCount; i++) {
            if (fanHasNft[i + 1][msg.sender]) {
                uint256 currentId = i + 1;
                Item storage currentItem = idToItem[currentId];
                items[currentIndex] = currentItem;
                currentIndex += 1;
            }
        }
        return items;
    }

    /* Returns only NFTs a user has created */
    function fetchNFTsCreated(address creator)
        public
        view
        returns (Item[] memory)
    {
        uint256 totalItemCount = _itemIds.current();
        uint256 itemCount = 0;
        uint256 currentIndex = 0;

        for (uint256 i = 0; i < totalItemCount; i++) {
            if (idToItem[i + 1].artist == creator) {
                itemCount += 1;
            }
        }

        Item[] memory items = new Item[](itemCount);
        for (uint256 i = 0; i < totalItemCount; i++) {
            if (idToItem[i + 1].artist == creator) {
                uint256 currentId = i + 1;
                Item storage currentItem = idToItem[currentId];
                items[currentIndex] = currentItem;
                currentIndex += 1;
            }
        }
        return items;
    }

    function pause() public onlyRole(PAUSER_ROLE) {
        _pause();
    }

    function unpause() public onlyRole(PAUSER_ROLE) {
        _unpause();
    }
}
