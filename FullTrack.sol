// SPDX-License-Identifier: MIT OR Apache-2.0
pragma solidity 0.8.3;

import "@openzeppelin/contracts-upgradeable/utils/CountersUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721URIStorageUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/ECDSAUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/draft-EIP712Upgradeable.sol";

contract FullTrack is
    ERC721URIStorageUpgradeable,
    PausableUpgradeable,
    AccessControlUpgradeable,
    OwnableUpgradeable,
    EIP712Upgradeable
{
    using CountersUpgradeable for CountersUpgradeable.Counter;
    using StringsUpgradeable for uint256;

    CountersUpgradeable.Counter private _tokenIds;

    address private nftCreatorAddress;
    address private artistAddress;
    string private baseURI;

    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    struct NFTData {
        string uri;
        bytes signature;
    }

    struct InitializationData {
        address kloovAdminAddress;
        address kloovPauserAddress;
        address kloovMinterAddress;
        address nftCreatorAddress;
        address artistAddress;
        string name;
        string symbol;
        string signingDomain;
        string signatureVersion;
        string tokenUri;
    }

    function initialize(InitializationData calldata initializationData)
        public
        initializer
        returns (bool)
    {
        __ERC721_init_unchained(
            initializationData.name,
            initializationData.symbol
        );
        __EIP712_init_unchained(
            initializationData.signingDomain,
            initializationData.signatureVersion
        );
        __Ownable_init_unchained();
        __Pausable_init_unchained();
        __AccessControl_init_unchained();

        nftCreatorAddress = initializationData.nftCreatorAddress;
        artistAddress = initializationData.artistAddress;
        baseURI = initializationData.tokenUri;

        _grantRole(DEFAULT_ADMIN_ROLE, initializationData.kloovAdminAddress);
        _grantRole(PAUSER_ROLE, initializationData.kloovPauserAddress);
        _grantRole(MINTER_ROLE, initializationData.kloovMinterAddress);
        _transferOwnership(initializationData.kloovMinterAddress);

        return true;
    }

    function redeem(
        address redeemer,
        uint256 tokenId,
        NFTData calldata tokenData
    ) public payable returns (bool) {
        address signer = _verify(tokenData);
        require(
            hasRole(MINTER_ROLE, signer),
            "FullTrack: Signature invalid or unauthorized"
        );
        _safeMint(signer, tokenId, "");
        _transfer(signer, redeemer, tokenId);

        return true;
    }

    function _hash(NFTData calldata tokenData) internal view returns (bytes32) {
        return
            _hashTypedDataV4(
                keccak256(
                    abi.encode(
                        keccak256("NFTData(string uri)"),
                        keccak256(bytes(tokenData.uri))
                    )
                )
            );
    }

    function _verify(NFTData calldata tokenData)
        internal
        view
        returns (address)
    {
        bytes32 digest = _hash(tokenData);
        return ECDSAUpgradeable.recover(digest, tokenData.signature);
    }

    function _baseURI() internal view override returns (string memory) {
        return baseURI;
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override
        returns (string memory)
    {
        return
            bytes(_baseURI()).length > 0
                ? string(
                    abi.encodePacked(
                        _baseURI(),
                        "/",
                        tokenId.toString(),
                        ".json"
                    )
                )
                : "";
    }

    function getArtist() public view returns (address) {
        return artistAddress;
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721Upgradeable, AccessControlUpgradeable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

    function pause() public onlyRole(PAUSER_ROLE) {
        _pause();
    }

    function unpause() public onlyRole(PAUSER_ROLE) {
        _unpause();
    }
}
